package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.util.RandomUUID;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class TaskService {
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectServise projectServise;
    private Scanner scanner;

    public TaskService(ProjectServise projectServise, Scanner scanner) {
        this.projectServise = projectServise;
        this.scanner = scanner;
    }

    public TaskService(ProjectServise projectServise) {
        this.projectServise = projectServise;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task findTaskByName(String taskName) {
        List<Task> tasks = new ArrayList<>(taskRepository.findAll().values());
        Task result = null;
        for (Task task : tasks) {
            if (task.getName().equals(taskName)) {
                result = task;
            }
        }
        if (result != null) return result;
        return null;
    }

    public void saveTask(String taskName, String projectName) {
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
        } else {
            String projectID = projectServise.fingProjectIdByName(projectName);
            if (!projectID.equals("")) {
                taskRepository.persist(new Task(taskName, RandomUUID.genRandomUUID(), projectID));
                System.out.println("[OK]");
            } else {
                System.out.println("[INCORRECT PROJECT NAME]");
            }

        }
    }

    public void removeTask(String taskName) {
        if (taskName == null || taskName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
        } else {
            taskRepository.remove(taskName);
            System.out.println("[OK]");
        }
    }

    public void showTasks() {
        if (taskRepository.isEmpty()) {
            System.out.println("[NO TASKS]");
        } else {
            int i = 1;
            for (Task task : taskRepository.findAll().values()) {
                System.out.println(i + ". " + task.getName());
            }
        }
    }

    public void clearTasks() {
        taskRepository.removeAll();
    }

    public void removeTaskForProject(String projectName) {
        if (projectName == null || projectName.isEmpty()) {
            return;
        } else {
            String projectId = projectServise.fingProjectIdByName(projectName);
            Iterator<Task> iterator = taskRepository.findAll().values().iterator();
            while (iterator.hasNext()) {
                Task task = iterator.next();
                if (task.getProjectId().equals(projectId)) {
                    taskRepository.remove(task.getName());
                }
            }
        }
    }

    public void update(String taskName) {
        if (taskName == null || taskName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        Task task = findTaskByName(taskName);
        if (task == null) {
            System.out.println("[NO SUCH TASK]");
            return;
        }
        System.out.println("[ENTER NEW NAME]");
        String newName = scanner.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        String description = scanner.nextLine();
        task.setName(newName);
        task.setDescription(description);
    }

    public void showTask(String taskName) {
        if (taskName == null || taskName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        Task task = findTaskByName(taskName);
        if (task != null) {
            System.out.println("[TASK INFORMATION]");
            System.out.println(task);
        }

    }
}
