package ru.sadkov.tm;

import ru.sadkov.tm.menu.Bootstrap;
import ru.sadkov.tm.menu.command.help.HelpCommand;
import ru.sadkov.tm.menu.command.projectCommand.*;
import ru.sadkov.tm.menu.command.taskCommand.*;

public class Application {
    private final static Class[] classes = {HelpCommand.class, ProjectCreateCommand.class, ProjectFindAllCOmmand.class,
            ProjectFindOneCommand.class, ProjectRemoveCommand.class, ProjectRemoveAllCommand.class,
            ProjectUpdateCommand.class, TaskCreateCommand.class, TaskRemoveCommand.class, TaskRemoveAllCommand.class,
            TaskUpdateCommand.class, TaskFindOneCommand.class, TaskFindAllCommand.class, TaskForProjectCommand.class};

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(classes);
    }
}

