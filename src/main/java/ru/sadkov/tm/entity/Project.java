package ru.sadkov.tm.entity;

import java.util.Date;

public class Project {
    private String Id;
    private String Name;
    private String description;
    private Date dateBegin;
    private Date dateEnd;

    public Project(String projectName, String projectId) {
        this.Name = projectName;
        this.Id = projectId;
    }

    public Project() {
    }

    @Override
    public String toString() {
        return "Project{" +
                "Id='" + Id + '\'' +
                ", Name='" + Name + '\'' +
                ", description='" + description + '\'' +
                ", dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getName() {
        return Name;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public void setName(String name) {
        this.Name = name;
    }


}
