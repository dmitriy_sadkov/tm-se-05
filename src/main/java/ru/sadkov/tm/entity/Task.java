package ru.sadkov.tm.entity;

import java.util.Date;

public class Task {
    private String Name;
    private String Id;
    private String description;
    private Date dateBegin;
    private Date dateEnd;
    private String projectId;

    public Task(String taskName, String taskId, String projectId) {
        this.Name = taskName;
        this.Id = taskId;
        this.projectId = projectId;
    }

    public Task() {
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TASK NAME: " + Name + "\n");
        sb.append("TASK ID: " + Id + "\n");
        sb.append("PROJECT ID: " + projectId + "\n");
        sb.append("DESCRIPTION: " + description + "\n");
        sb.append("DATE BEGIN: " + dateBegin + "\n");
        sb.append("DATE END: " + dateEnd + "\n\n");
        return sb.toString();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }
}
