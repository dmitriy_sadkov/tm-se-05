package ru.sadkov.tm.repository;

import ru.sadkov.tm.entity.Task;

import java.util.*;

public class TaskRepository {
    private Map<String, Task> taskList = new LinkedHashMap<>();

    public boolean isEmpty() {
        return taskList.isEmpty();
    }

    public Map<String, Task> findAll() {
        return taskList;
    }


    public void persist(Task task) {
        taskList.put(task.getId(), task);
    }

    public void remove(String taskName) {
        taskList.values().removeIf(nextTask -> nextTask.getName().equalsIgnoreCase(taskName));
    }

    public void removeAll() {
        taskList.clear();
    }

    public Task findOne(String taskId) {
        return taskList.get(taskId);
    }

    public void merge(Task task) {
        if (taskList.containsValue(task)) {
            update(task.getId(), task.getName());
        } else {
            persist(task);
        }
    }

    public void update(String taskId, String taskName) {
        taskList.get(taskId).setName(taskName);
    }
}
