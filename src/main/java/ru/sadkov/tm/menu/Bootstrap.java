package ru.sadkov.tm.menu;

import ru.sadkov.tm.menu.command.AbstractCommand;
import ru.sadkov.tm.service.ProjectServise;
import ru.sadkov.tm.service.TaskService;

import java.util.*;

public class Bootstrap {
    private Scanner scanner = new Scanner(System.in);
    private ProjectServise projectService = new ProjectServise(scanner);
    private TaskService taskServise = new TaskService(projectService, scanner);
    private Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commandMap.values());
    }

    public void setCommandMap(Map<String, AbstractCommand> commandMap) {
        this.commandMap = commandMap;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public ProjectServise getProjectService() {
        return projectService;
    }

    public void setProjectService(ProjectServise projectService) {
        this.projectService = projectService;
    }

    public TaskService getTaskServise() {
        return taskServise;
    }

    public void setTaskServise(TaskService taskServise) {
        this.taskServise = taskServise;
    }

    private void registry(Class clazz) throws IllegalAccessException, InstantiationException {
        AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        final String commandName = abstractCommand.command();
        final String commandDescription = abstractCommand.description();
        if (commandName == null || commandName.isEmpty()) return;
        if (commandDescription == null || commandDescription.isEmpty()) return;
        abstractCommand.setBootstrap(this);
        commandMap.put(commandName, abstractCommand);
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("enter HELP for command list");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commandMap.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public void init(final Class... commandsClasses) {
        if (commandsClasses == null || commandsClasses.length == 0) return;
        for (Class clazz : commandsClasses) {
            try {
                registry(clazz);
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
        try {
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
