package ru.sadkov.tm.menu.command.help;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {
    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() throws Exception {
        for (final AbstractCommand command:
                bootstrap.getCommandList()) {
            System.out.println(command.command() + ": "
                    + command.description());
        }
    }
}
