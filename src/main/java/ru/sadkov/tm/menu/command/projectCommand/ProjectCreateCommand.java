package ru.sadkov.tm.menu.command.projectCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        String projectName = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().saveProject(projectName);
    }
}
