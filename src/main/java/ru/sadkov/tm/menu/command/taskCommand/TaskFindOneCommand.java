package ru.sadkov.tm.menu.command.taskCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class TaskFindOneCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-show";
    }

    @Override
    public String description() {
        return "Show task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER TASK NAME]");
        String taskName = bootstrap.getScanner().nextLine();
        bootstrap.getTaskServise().showTask(taskName);
    }
}
