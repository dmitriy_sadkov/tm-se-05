package ru.sadkov.tm.menu.command.taskCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("[ENTER NAME:]");
        String taskName = bootstrap.getScanner().nextLine();
        bootstrap.getTaskServise().removeTask(taskName);
    }
}
