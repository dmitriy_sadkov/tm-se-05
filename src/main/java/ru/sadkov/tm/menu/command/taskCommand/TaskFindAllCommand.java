package ru.sadkov.tm.menu.command.taskCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "show-all-task";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS:]");
        bootstrap.getTaskServise().showTasks();
    }
}
