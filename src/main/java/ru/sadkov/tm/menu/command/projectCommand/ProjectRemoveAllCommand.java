package ru.sadkov.tm.menu.command.projectCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class ProjectRemoveAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS CLEAR]");
        bootstrap.getProjectService().clearProjects();
        bootstrap.getTaskServise().clearTasks();
    }
}
