package ru.sadkov.tm.menu.command;

import ru.sadkov.tm.menu.Bootstrap;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;

    public AbstractCommand() {
    }

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;
}
