package ru.sadkov.tm.menu.command.projectCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("[ENTER NAME:]");
        String projectName = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().removeProject(projectName);
        bootstrap.getTaskServise().removeTaskForProject(projectName);
    }
}
