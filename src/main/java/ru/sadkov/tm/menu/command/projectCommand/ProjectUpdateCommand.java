package ru.sadkov.tm.menu.command.projectCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class ProjectUpdateCommand extends AbstractCommand {


    @Override
    public String command() {
        return "project-update";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().update(projectName);
    }
}
