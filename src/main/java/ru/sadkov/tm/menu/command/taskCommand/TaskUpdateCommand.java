package ru.sadkov.tm.menu.command.taskCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-update";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER TASK NAME]");
        String taskName = bootstrap.getScanner().nextLine();
        bootstrap.getTaskServise().update(taskName);
    }
}
