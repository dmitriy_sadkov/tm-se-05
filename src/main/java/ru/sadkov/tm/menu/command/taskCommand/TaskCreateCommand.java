package ru.sadkov.tm.menu.command.taskCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        String taskName = bootstrap.getScanner().nextLine();
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = bootstrap.getScanner().nextLine();
        bootstrap.getTaskServise().saveTask(taskName, projectName);
    }
}
