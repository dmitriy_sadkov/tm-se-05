package ru.sadkov.tm.menu.command.projectCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class ProjectFindOneCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-show";
    }

    @Override
    public String description() {
        return "Show project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().showProject(projectName);
    }
}
