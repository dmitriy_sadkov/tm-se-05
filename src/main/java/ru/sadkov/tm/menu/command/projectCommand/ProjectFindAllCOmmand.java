package ru.sadkov.tm.menu.command.projectCommand;

import ru.sadkov.tm.menu.command.AbstractCommand;

public class ProjectFindAllCOmmand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS:]");
        bootstrap.getProjectService().showProjects();
    }
}
