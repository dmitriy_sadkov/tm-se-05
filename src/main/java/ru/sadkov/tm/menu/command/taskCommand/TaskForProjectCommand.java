package ru.sadkov.tm.menu.command.taskCommand;

import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.menu.command.AbstractCommand;
import ru.sadkov.tm.util.ShowList;

import java.util.ArrayList;
import java.util.List;

public class TaskForProjectCommand extends AbstractCommand {
    @Override
    public String command() {
        return "tasks-for-project";
    }

    @Override
    public String description() {
        return "Show tasks for chosen project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS FOR PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = bootstrap.getScanner().nextLine();
        String projectId = bootstrap.getProjectService().fingProjectIdByName(projectName);
        if (projectId == null || projectId.isEmpty()) {
            System.out.println("[INVALID PROJECT NAME]");
            return;
        }
        List<Task> result = new ArrayList<>();
        for (Task task : bootstrap.getTaskServise().getTaskRepository().findAll().values()) {
            if (task.getProjectId() != null && task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        if (result.isEmpty()) {
            System.out.println("[NO TASKS IN PROJECT]");
        } else {
            ShowList.showList(result);
        }
    }
}
